
it('CSAT', function() {
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('superadmin');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word{enter}');
  cy.get('button').click();
  cy.get('app-inner-nav > .nav__left > ol > :nth-child(3) > a').click();
  cy.get('[routerlink="platform/customer-satisfaction/surveys/active-surveys"] > span').click();
  cy.get('.btn').click();
  cy.get(':nth-child(1) > .survey-form-input__field > .ng-untouched').clear();
  cy.get(':nth-child(1) > .survey-form-input__field > .ng-untouched').type('CSAT2');
  cy.get(':nth-child(2) > .survey-form-input > .survey-form-input__field > .ng-untouched').click();
  cy.get('.survey-form-input__field > .ng-untouched').clear();
  cy.get('.survey-form-input__field > .ng-untouched').type('Hello there! How did I do? $$options Thanks! :) $$agentname');
  cy.get(':nth-child(2) > .panel-input--note > .ql-container > .ql-editor').click();
  cy.get(':nth-child(2) > .panel-input--note > .ql-container > .ql-editor').type('$$options thanks you for your feedback.');
  cy.get(':nth-child(3) > .panel-input--note > .ql-container > .ql-editor > p').click();
  cy.get('.dropdown-label').click();
  cy.get(':nth-child(3) > .mat-list-item-content > .mat-list-text').click();
  cy.get('.menu-actions > .btn').click();
  cy.get('.btn--save').click();
  cy.get('.presence-current__arrow').click();
  cy.get('.presence__status--choice').click({force: true});
});
