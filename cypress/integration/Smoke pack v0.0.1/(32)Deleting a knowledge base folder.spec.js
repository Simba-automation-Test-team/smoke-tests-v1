
it('Deleting knowledge base Folder', function() {
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('superadmin');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click();
  cy.get(':nth-child(5) > a').click();
  cy.get('[routerlink="./knowledge-base"] > span').click();

  /* ==== Generated with Cypress Studio ==== */
  cy.get('.header > app-create-folder > div > .create-folder').click();
  cy.get('.create-folder__input').clear();
  cy.get('.create-folder__input').type('Smoke test folder');
  cy.get('.new-folder > .btn').click().wait(1000);
  cy.contains('Smoke test folder').click();
  cy.get('[mattooltip="Delete folder"] > .fas').click();
  cy.get('.btn--confirm').trigger('click');
  cy.get('.btn--confirm').click();

  cy.get('.presence__status--choice').click({force: true});

});

/* cy.get('.header > app-create-folder > div > .create-folder > .create-folder__icon > .far').click()
  /html/body/div[3]/div[2]/div/mat-dialog-container/app-confirm-dialog/div[2]/button[2]

//*[@id="mat-dialog-6"]/app-confirm-dialog/div[2]/button[2]
  
  document.querySelector("#mat-dialog-6 > app-confirm-dialog > div.mat-dialog-actions > button.btn.btn--small.btn--confirm")
  cy.get(':nth-child(2) > [fxlayout="column drag"] > [_ngcontent-mvd-c320=""][fxlayoutalign="flex-start center"] > app-edit-folder-name > app-inplace > .inplace > .inplace-display > [_ngcontent-mvd-c313=""] > .cursor-pointer').click({force: true});
  cy.get('[_ngcontent-mvd-c313=""] > [fxlayoutalign="flex-start center"] > .ng-star-inserted > .fas').click();
  cy.get('.btn--confirm').click();
  #mat-dialog-6 > app-confirm-dialog > div.mat-dialog-actions > button.btn.btn--small.btn--confirm
  cy.get('.presence__status--choice').click({force: true});
*/