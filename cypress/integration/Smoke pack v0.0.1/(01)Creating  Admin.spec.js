// DefaultAdminUser_Admin.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
it('Creating Admin', function() {
cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
cy.get('#mat-input-0').click();
cy.get('#mat-input-0').type('superadmin');
cy.get('#mat-input-1').click();
cy.get('#mat-input-1').type('Pa55word');
cy.get('button').click();
cy.get(':nth-child(1) > a > .nav-item__arrow').click();
cy.get('[routerlink="people/users"] > span').click();
cy.get('.btn').click();
cy.get('#firstName').click();
cy.get('#firstName').type('Tony');
cy.get('#lastName').click();
cy.get('#lastName').type('Romero');
cy.get('#userName').click();
cy.get('#userName').type(userID_Alpha())
    function userID_Alpha() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    
        for (var i = 0; i < 10; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));
    
        return text;
      } 



cy.get('#emailAddress').click();
cy.get('#emailAddress').type(makeemail(6) + '@TextDecoderStream.com')
    function makeemail() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    
        for (var i = 0; i < 10; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));
    
        return text;
      } 




cy.get('.roles-container .placeholder-text').click();
cy.get('#mat-checkbox-1 .mat-checkbox-inner-container').click();
cy.get('.btn--action').click();
cy.get('.btn:nth-child(2)').click();
cy.get('.presence__status--choice').click({force: true});
});