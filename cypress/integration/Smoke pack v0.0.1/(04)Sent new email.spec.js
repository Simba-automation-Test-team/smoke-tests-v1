

describe('Send a New Email to Consumer', function () {
    it('Send new Email', function () {
        cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
        cy.get('#mat-input-0').clear();
        cy.get('#mat-input-0').type('agentf');
        cy.get('#mat-input-1').clear();
        cy.get('#mat-input-1').type('Pa55word{enter}');
        cy.get('button').click();
        //  logging in
        cy.wait(500);
        cy.get('.fa-plus').click();
        cy.get('.channel > .far').click();
        //  Opening a new email
        cy.get('#mat-input-3').clear();
        cy.get('#mat-input-3').type('jordan@stellaruk.co.uk');
        //  inputting email of consumer
        cy.get('#mat-input-4').clear();
        cy.get('#mat-input-4').type('Cypress Test');
        //  Subject line
        cy.get('div.ql-editor').type('Cypress Test');
        //  Entering text for main body of email
        cy.get('.splitbutton-button').click({force: true});
        //  Sending email
        cy.get('.presence__status--choice').click({force: true});
        //  Logging out
    })
})
