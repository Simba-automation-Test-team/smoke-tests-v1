// ReRouteConversation_Agent.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
/* ==== Test Created with Cypress Studio ==== */
it('ReReoute Conversation', function() {
  /* ==== Generated with Cypress Studio ==== */
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('agentf');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click();
  cy.get('.delay-550 > .conversation-list-item__wrapper > .conversation-list-item__container > .conversation-details > .details__conversation-subject').click();
  cy.get('.delay-550 > .conversation-list-item__wrapper > .conversation-list-item__container > .conversation-list-item__actions > :nth-child(2) > img').click();
  cy.get('.queue-list > :nth-child(11)').click();
  cy.get('.presence__status--choice').click({force: true});
  /* ==== End Cypress Studio ==== */
});
