describe('Search for Waiting Conversations', function() {
    it('search by waiting conversation', function() {
        cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
        cy.get('#mat-input-0').clear();
        cy.get('#mat-input-0').type('agentf');
        cy.get('#mat-input-1').clear();
        cy.get('#mat-input-1').type('Pa55word{enter}');
        cy.get('button').click();
        //  Logging in
        cy.get('.search-button').click();
        cy.get('#mat-expansion-panel-header-0 > .mat-content > .mat-expansion-panel-header-title').click();
        //  Navigating to search and conversations
        cy.get(':nth-child(2) > .filter--checkbox').uncheck();
        cy.get(':nth-child(3) > .filter--checkbox').uncheck();
        cy.get('.filter--fieldset.ng-dirty > :nth-child(5)').click();
        cy.get(':nth-child(5) > .filter--checkbox').uncheck();
        cy.get('.filter--fieldset.ng-dirty > :nth-child(6)').click();
        cy.get(':nth-child(6) > .filter--checkbox').uncheck();
        cy.get(':nth-child(7) > .filter--checkbox').uncheck();
        //  Unchecking filters that we do not need i.e. open, new, closed etc
        cy.get('app-conversations-filter > .filter > .filter--buttons > .btn--save').click();
         //  Searching for new conversations
        cy.wait(1000)
        cy.get('.presence__status--choice').click({force: true});
          //  Logging out
    })
})
