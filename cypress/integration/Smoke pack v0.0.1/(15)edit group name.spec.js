describe('Edit Group Name', function() {
    it('edit group name', function() {
        cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
        cy.get('#mat-input-0').clear();
        cy.get('#mat-input-0').type('superadmin');
        cy.get('#mat-input-1').clear();
        cy.get('#mat-input-1').type('Pa55word{enter}');
        cy.get('button').click();
        //  Logging in
        cy.get('app-inner-nav > .nav__left > ol > :nth-child(1) > a').click();
        cy.get('[routerlink="people/groups"] > span').click();
        cy.get('.cdk-focused').click();
        //  Navigating and entering Groups
        cy.wait(500),
        cy.get(':nth-child(17) > .cdk-column-actions > .actions > [style="margin-right: 15px;"] > .mat-tooltip-trigger').click();
        cy.get('.edit-form').click();
        cy.get('.group-name > .input--default').clear();
        cy.get('.group-name > .input--default').type('Cypress Test(Munya Group)');
        cy.get('[type="submit"]').click();
        //  Editing and submiting changes
        cy.get('.presence__status--choice').click({force: true});
        //  Logging out
    })
})
