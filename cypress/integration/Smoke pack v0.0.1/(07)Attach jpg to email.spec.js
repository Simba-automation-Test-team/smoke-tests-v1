




// require('cypress-file-upload')
//  Added so the file upload commands are recognised as this is a plugin not native to Cypress/



describe('Attach jpg to email', function() {
    it('Uploads an attachment agent side', function() {
        
        
        
        cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
        cy.get('#mat-input-0').clear();
        cy.get('#mat-input-0').type('agentf');
        cy.get('#mat-input-1').clear();
        cy.get('#mat-input-1').type('Pa55word{enter}');
        cy.get('button').click();
        
        //*  Steps to Log in
        
        cy.get('.delay-0 > .conversation-list-item__wrapper > .conversation-list-item__container > .conversation-details > .details__conversation-subject > .ng-star-inserted').click();
        cy.get('label > .mat-tooltip-trigger').click();
        
        //*  Navigating to conversations
        
        cy.wait(500)
        const filepath = 'images/test.jpg'
        
        //*  This is stating the path to the file
        //*  so basically Cypress > Fixtures > Images > test.jpg
        
        cy.get('input[type="file"]').attachFile(filepath)
        
        //* This is getting the file and uploading/attaching the file
        
        cy.get('.ql-container > .ql-editor').click();
        cy.get('.ql-container > .ql-editor').type('Agent attachment');
        
        //*  Adding text to main body of the email
        
        cy.wait(1000)
        
        //*  Wait just to verify the file has been uploaded
        
        cy.get('.btn-send__content').click();
        
        //*  Sending the email
        
        cy.wait(1000)
        
        //  Waiting to ensure the email is sent successfully 
        
        cy.get('.presence__status--choice').click({force: true});
        
        //  Logging out
    })
})
