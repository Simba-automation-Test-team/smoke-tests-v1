describe('KB Item in Email', function() {
    it('kb item in email', function () {
        cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
        cy.get('#mat-input-0').clear();
        cy.get('#mat-input-0').type('agentf');
        cy.get('#mat-input-1').clear();
        cy.get('#mat-input-1').type('Pa55word');
        cy.get('button').click();
        //  Logging out
        cy.get('.delay-0 > .conversation-list-item__wrapper > .conversation-list-item__container > .conversation-details > .details__conversation-subject > .ng-star-inserted').click();
        cy.get('[routerlink="knowledge-base"] > .conversation-tab__header > .fas').click();
        cy.get(':nth-child(1) > [style="padding: 3px 0;"] > div.mat-tree-node > .mat-focus-indicator > .mat-button-wrapper > .mat-icon').click();
        cy.get('[aria-expanded="true"] > [style="padding: 3px 0;"] > ul > :nth-child(1) > .mat-tree-node > .cursor-pointer').click();
        cy.wait(500)
        cy.get('.article__footer > .btn').click();
        //  Navigating to KB list and selecting an item
        cy.get('.btn-send__content').click();
        //  Sending email
        cy.get('.presence__status--choice').click({force: true});
        //  Logging out
    })
})
