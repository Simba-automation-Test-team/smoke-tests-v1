
it('Edit skill', function() {
  
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('superadmin');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click();
  cy.get(':nth-child(1) > a > .nav-item__arrow').click();
  cy.get('[routerlink="people/manage-skills"] > span').click();
  cy.get(':nth-child(2) > .cdk-column-actions > .actions > [style="margin-right: 15px;"] > .mat-tooltip-trigger').click({force: true});
  cy.get('form.ng-untouched > :nth-child(2) > .input--default').click().type('test');
  cy.get('[type="submit"]').click();
  cy.get('.presence__status--choice').click({force: true});

});
