
it('Webchat', function() {
    
    cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
    cy.get('#mat-input-0').clear();
    cy.get('#mat-input-0').type('superadmin');
    cy.get('#mat-input-1').clear();
    cy.get('#mat-input-1').type('Pa55word{enter}');
    cy.get('button').click();
    cy.get('app-inner-nav > .nav__left > ol > :nth-child(3) > a').click();
    cy.get('[routerlink="platform/manage-channels"] > span').click();
    cy.get('app-webchat-channels.ng-star-inserted > .table-row__wrapper > .contains-list > :nth-child(3) > .quick-action > .quick-action__icon > .mat-tooltip-trigger').click();
    cy.get('.ng-untouched').clear();
    cy.get('.ng-untouched').type(userID_Alpha())
    function userID_Alpha() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    
        for (var i = 0; i < 10; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));
    
        return text;
      } 
    
    
    
    cy.get('.next-btn').click();
    cy.get('.row--stretch > .ng-untouched').click().type('hello');
    cy.get('.queue-choice > span').click();
    cy.get('.new-queue > :nth-child(2) > span').click();
    cy.get('.save__container > .ng-star-inserted').click();
    cy.get('.presence__status--choice').click({force: true});
    
  });

  
  
  
  
  
  
  
  
  
  