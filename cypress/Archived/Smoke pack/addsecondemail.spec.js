describe('Adding a second email to consumer profile', function() {
    it('Adds a second email to consumer profile', function() {
        cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
        cy.get('#mat-input-0').clear();
        cy.get('#mat-input-0').type('agentf');
        cy.get('#mat-input-1').clear();
        cy.get('#mat-input-1').type('Pa55word');
        cy.get('button').click();
        //  Logging in
        cy.get('.delay-0 > .conversation-list-item__wrapper > .conversation-list-item__container > .conversation-details > .details__conversation-subject > .ng-star-inserted').click();
        cy.get('.consumer-profile-widget > .btn').click();
        //  Navigating to conversations and opening Consumer profile
        cy.get('.customer-information > :nth-child(6)').click();
        cy.get(':nth-child(6) > div.ng-untouched > :nth-child(2) > .ng-untouched').select('OTHER');
        cy.get(':nth-child(6) > div.ng-untouched > :nth-child(3) > .input--default').click();
        cy.get(':nth-child(6) > div.ng-valid > :nth-child(3) > .input--default').clear();
        cy.get(':nth-child(6) > div.ng-valid > :nth-child(3) > .input--default').type('JordanStellarTest@outlook.com');
        cy.get('.consumer-edit > :nth-child(4)').click();
        //  Editing Consumer profile and adding a second email
        cy.get('.presence__status--choice').click({force: true});
        //  Logging out
    })
})