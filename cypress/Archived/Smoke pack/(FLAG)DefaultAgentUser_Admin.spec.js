// DefaultAgentUser_Admin.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
it('newtest', function() {
cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
cy.get('#mat-input-0').click();
cy.get('#mat-input-0').type('superadmin');
cy.get('#mat-input-1').click();
cy.get('#mat-input-1').type('Pa55word');
cy.get('button').click();
cy.get(':nth-child(1) > a > .nav-item__arrow').click();
cy.get('[routerlink="people/users"] > span').click();
cy.get('.btn').click();
cy.get('#firstName').click();
cy.get('#firstName').type('John');
cy.get('#lastName').click();
cy.get('#lastName').type('Jay');
cy.get('#userName').click();
cy.get('#userName').type('JohnJay');
cy.get('#emailAddress').click();
cy.get('#emailAddress').type('JJ@gmail.com');
cy.get('.roles-container .placeholder-text').click();
cy.get('#mat-checkbox-3 .mat-checkbox-inner-container').click();
cy.get('.btn--action').click();
cy.get('.btn:nth-child(2)').click();
cy.get('.presence__status--choice').click({force: true});
});