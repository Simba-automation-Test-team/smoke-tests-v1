
it('Note', function() {
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('agentf');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word{enter}');
  cy.get('button').click();
  cy.get('.delay-50 > .conversation-list-item__wrapper > .conversation-list-item__container > .conversation-details > .conversation-details__top > .conversation-focus-details > .conversation-sla > app-sla-timer > .sla-timer > :nth-child(1) > span').click();
  cy.get('.response-type__icon > .fal').click();
  cy.get(':nth-child(2) > .channels__item__icon').click();
  cy.get('.ql-container > .ql-editor').click();
  cy.get('.btn-send > span').click();
  cy.wait(500);
  cy.get('.presence__status--choice').click({force: true});
  
});
