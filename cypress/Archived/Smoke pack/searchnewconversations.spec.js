describe('Search for New Conversations', function() {
    it('Makes an Assertion', function() {
        cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
        cy.get('#mat-input-0').clear();
        cy.get('#mat-input-0').type('superadmin');
        cy.get('#mat-input-1').clear();
        cy.get('#mat-input-1').type('Pa55word{enter}');
        cy.get('button').click();
        //  Steps to Login
        cy.get('.search-button').click();
        cy.get('#mat-expansion-panel-header-0 > .mat-content').click();
        cy.get('[formarrayname="conversationStatuses"] > :nth-child(3)').click();
        //  Navigating to Conversations
        cy.get(':nth-child(3) > .filter--checkbox').uncheck();
        cy.get('.filter--fieldset.ng-dirty > :nth-child(4)').click();
        cy.get(':nth-child(4) > .filter--checkbox').uncheck();
        cy.get('.filter--fieldset.ng-dirty > :nth-child(7)').click();
        cy.get(':nth-child(7) > .filter--checkbox').uncheck();
        cy.get('.filter--fieldset.ng-dirty > :nth-child(6)').click();
        cy.get(':nth-child(6) > .filter--checkbox').uncheck();
        cy.get('.filter--fieldset.ng-dirty > :nth-child(5)').click();
        cy.get(':nth-child(5) > .filter--checkbox').uncheck();
        //  Unchecking filters that we do not need i.e. open, waiting etc
        cy.get('app-conversations-filter > .filter > .filter--buttons > .btn--save').click();
        //  Searching for new conversations
        cy.wait(1000)
        cy.get('.presence__status--choice').click({force: true})
        //  Logging out
    })
})
