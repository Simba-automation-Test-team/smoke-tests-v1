// Groups 2.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
/* ==== Test Created with Cypress Studio ==== */
it('Creating group 2', function() {
  /* ==== Generated with Cypress Studio ==== */
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('superadmin');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click();
  cy.get(':nth-child(2) > .mat-tooltip-trigger > a > .fas').click();
  cy.get(':nth-child(1) > a > .nav-item__arrow').click();
  cy.get('[routerlink="people/groups"] > span').click();
  cy.get('.btn').click();
  cy.get('.group-name > .input--default').clear();
  cy.get('.group-name > .input--default').type('munya group ');
  cy.get('.link-queue > .dropdown > .dropdown-trigger > .dropdown-label > .ng-star-inserted').click();
  cy.get('.link-queue > .dropdown > .menu > .menu-content > .mat-selection-list > :nth-child(11) > .mat-list-item-content > .mat-pseudo-checkbox').click();
  cy.get('.link-queue > .dropdown > .menu > .p-10 > .btn').click();
  cy.get('.dropdown-label > div.ng-star-inserted').click();
  cy.get('.users > .dropdown > .menu > .menu-content > .mat-selection-list > :nth-child(9) > .mat-list-item-content > .mat-pseudo-checkbox').click();
  cy.get('.users > .dropdown > .menu > .p-10 > .btn').click();
  cy.get('[type="submit"]').click();
  cy.get('.subnav--active > a > .nav-item__arrow').click();
  /* ==== End Cypress Studio ==== */
});
