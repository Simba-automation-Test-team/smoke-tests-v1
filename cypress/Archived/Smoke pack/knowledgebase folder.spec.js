it('Knowledgebase folder ', function() {
    cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
    cy.get('#mat-input-0').clear();
    cy.get('#mat-input-0').type('superadmin');
    cy.get('#mat-input-1').clear();
    cy.get('#mat-input-1').type('Pa55word{enter}');
    cy.get('button').click();
    cy.get(':nth-child(5) > a').click();
    cy.get('[routerlink="./knowledge-base"] > span').click();
    cy.get('.header > app-create-folder > div > .create-folder > .create-folder__label').click();
    cy.get('.create-folder__input').clear();
    cy.get('.create-folder__input').type('Name here');
    cy.get('.new-folder > .btn').click();
    cy.get('.presence__status--choice').click({force: true});
    });
  