describe('Updates a Consumer email', function() {
    it('Change the main email of a consumer', function() {
        cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
        cy.get('#mat-input-0').clear();
        cy.get('#mat-input-0').type('agentf');
        cy.get('#mat-input-1').clear();
        cy.get('#mat-input-1').type('Pa55word{enter}');
        cy.get('button').click();
        //  Logging in
        cy.get('.delay-0 > .conversation-list-item__wrapper > .conversation-list-item__container > .conversation-details > .details__conversation-subject > .ng-star-inserted').click();
        cy.get('.consumer-profile-widget > .btn').click();
        //  Navigating to conversations and opening Consumer profile
        cy.get('#widget-interaction-wrapper').click();
        cy.get('div.ng-untouched > :nth-child(3) > .input--default').clear().type('JordanStellarTest@outlook.com');
        //  Deletes the current text then adds the specified 
        cy.get('.consumer-edit > :nth-child(4)').click();
        //  Changing main email and saving changes
        cy.wait(1000)
        cy.get('.presence__status--choice').click({force: true});
        //  Logging out
    })
})