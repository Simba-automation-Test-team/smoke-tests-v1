// DraftEmail.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
/* ==== Test Created with Cypress Studio ==== */
it('Draft email test', function() {
  /* ==== Generated with Cypress Studio ==== */
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('agentf');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word{enter}');
  cy.get('button').click();
  cy.get('.dock__btn').click({force: true});
  cy.get('.channel > .far').click({force: true}); 
  cy.get('#mat-input-3').clear();
  cy.get('#mat-input-3').type('kieranwilson@stellaruk.co.uk');
  cy.get('#mat-input-4').clear();
  cy.get('#mat-input-4').type('Cypress Draft Test');
  cy.get('.compose-email__controls > .far').click();
  cy.get('.mat-menu-trigger').click();
  cy.get('.cdk-overlay-backdrop').click();
  cy.get('.logout').click({force:true});
  /* ==== End Cypress Studio ==== */
});
