


it('Retire', function() {
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('superadmin');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click();
  cy.get('app-inner-nav > .nav__left > ol > :nth-child(1) > a').click();
  cy.get('[routerlink="people/manage-skills"] > span').click().wait(500);
  cy.get('.btn').click();
  cy.get(':nth-child(1) > .input--default').clear();
  cy.get(':nth-child(1) > .input--default').type(userID_Alpha())
  function userID_Alpha() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  
      for (var i = 0; i < 10; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
  
      return text;
    } 
  cy.get('form.ng-invalid > :nth-child(2) > .input--default').type('delte this after');
  cy.get('.ng-invalid.ng-dirty > :nth-child(2) > .input--default').click();
  cy.get('.w-100 > .dropdown > .dropdown-trigger > .dropdown-label').click();
  cy.get('#mat-radio-2 > .mat-radio-label > .mat-radio-label-content').click();
  cy.get('#mat-radio-2-input').check();
  cy.get('.menu-actions > .btn').click();
  cy.get('.users > .dropdown > .dropdown-trigger > .dropdown-label > div.ng-star-inserted > .ng-star-inserted').click();
  cy.get('.users > .dropdown > .menu > .menu-content > .mat-selection-list > :nth-child(2) > .mat-list-item-content > .mat-pseudo-checkbox').click();
  cy.get('.users > .dropdown > .menu > .p-10 > .btn').click();
  cy.get('.queues > .dropdown > .dropdown-trigger').click();
  cy.get('.queues > .dropdown > .menu > .menu-content > .mat-selection-list > :nth-child(2) > .mat-list-item-content > .mat-pseudo-checkbox').click();
  cy.get('.queues > .dropdown > .menu > .p-10 > .btn').click();
  cy.get('[type="submit"]').click();
  cy.get(':nth-child(2) > .cdk-column-actions > .actions > :nth-child(2) > .mat-tooltip-trigger').click({force: true});
  cy.get('.btn--confirm').click();
  cy.wait(1000)
  cy.get('.presence__status--choice').click({force: true});

});

