// SearchNewConversation_Agent.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
/* ==== Test Created with Cypress Studio ==== */
it('NewConversation', function() {
  /* ==== Generated with Cypress Studio ==== */
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('agentf');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click();
  cy.get('.search-icon').click();
  cy.get('#mat-expansion-panel-header-0 > .mat-content > .mat-expansion-panel-header-title').click();
  cy.get(':nth-child(5) > .filter--checkbox').uncheck();
  cy.get(':nth-child(6) > .filter--checkbox').uncheck();
  cy.get('.filter--fieldset.ng-dirty > :nth-child(3)').click();
  cy.get(':nth-child(3) > .filter--checkbox').uncheck();
  cy.get('.filter--fieldset.ng-dirty > :nth-child(4)').click();
  cy.get(':nth-child(4) > .filter--checkbox').uncheck();
  cy.get('.filter--fieldset.ng-dirty > :nth-child(7)').click();
  cy.get(':nth-child(7) > .filter--checkbox').uncheck();
  cy.get('app-conversations-filter > .filter > .filter--buttons > .btn--save').click();
  cy.get('.results-item--column--data > :nth-child(1)');
  cy.get('.logout').click({force: true});
  /* ==== End Cypress Studio ==== */
});
