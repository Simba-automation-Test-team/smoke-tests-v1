// EditNumber_Agent.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
it('Edit', function() {
    cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
    cy.get('#mat-input-0').click();
    cy.get('#mat-input-0').type('agentf');
    cy.get('#mat-input-1').click();
    cy.get('#mat-input-1').type('Pa55word');
    cy.get('button').click();
    cy.get('.fa-user-friends').click();
    cy.get('.delay-0 .conversation-details').click();
    cy.get('.btn').click();
    cy.get('.form-element:nth-child(4) .input--default').click();
    cy.get('.form-element:nth-child(4) .input--default').type('{backspace}');
    cy.get('.mini > .ng-dirty').type('3');
    cy.get('.btn:nth-child(4)').click();
    cy.get('.presence__status--choice').click({force: true});
    });