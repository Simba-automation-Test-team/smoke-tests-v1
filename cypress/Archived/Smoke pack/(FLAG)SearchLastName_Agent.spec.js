// SearchLastName_Agent.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
/* ==== Test Created with Cypress Studio ==== */
it('LastName', function() {
  /* ==== Generated with Cypress Studio ==== */
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('agentf');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click();
  cy.get('.search-button').click();
  cy.get('#mat-expansion-panel-header-2 > .mat-content > .mat-expansion-panel-header-title').click();
  cy.get('app-consumers-filter > .filter > :nth-child(2) > .input--default').clear();
  cy.get('app-consumers-filter > .filter > :nth-child(2) > .input--default').type('Lyall{enter}');
  cy.get('app-consumers-filter > .filter > .filter--buttons > .btn--save').click();
  cy.get('.results-item--column-70').click();
  cy.get('.presence__status--choice').click({force: true});
  /* ==== End Cypress Studio ==== */
});
