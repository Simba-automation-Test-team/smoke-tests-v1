// OpenCoversations_Agent.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
/* ==== Test Created with Cypress Studio ==== */
it('calander test', function() {
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login/');
  //*login sequence
  cy.get('#mat-input-0').type('superadmin');
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click();
  //*Navigation to the filter function
  cy.get('.search-icon').click();
  cy.get('#mat-expansion-panel-header-0 > .mat-content > .mat-expansion-panel-header-title').click();
  //*Chossing the requirements for search E.G. open, closed ect 
  cy.get(':nth-child(6) > .filter--checkbox').uncheck();
  cy.get(':nth-child(5) > .filter--checkbox').uncheck();
  cy.get(':nth-child(2) > .filter--checkbox').uncheck();
  cy.get(':nth-child(7) > .filter--checkbox').uncheck();
  cy.get(':nth-child(4) > .filter--checkbox').uncheck();
  //*Givving the dates, below you must change the date to be within the 3 month requirement
  cy.get('[formcontrolname="from"]').type('2021-07-01');
  cy.get('app-conversations-filter > .filter > .filter--buttons > .btn--save').click({force: true});
  //*Logout sequence
  cy.get('.logout').click({force: true});
  });
