// Tags 1.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
/* ==== Test Created with Cypress Studio ==== */
it('tags 1', function() {
  /* ==== Generated with Cypress Studio ==== */
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('superadmin');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click();
  cy.get('[mattooltip="Business Rules"] > a > .nav-item__arrow').click();
  cy.get(':nth-child(9) > .subnav__list > .mat-tooltip-trigger > span').click();
  cy.get(':nth-child(9) > .subnav__list > .mat-tooltip-trigger > span').click();
  cy.get('.admin-header__content--right > .btn').click();
  cy.get(':nth-child(1) > .input--default').clear();
  cy.get(':nth-child(1) > .input--default').type('collection');
  cy.get('.mat-chip-list-wrapper').click();
  cy.get('#mat-chip-list-input-0').clear();
  cy.get('#mat-chip-list-input-0').type('5{enter}');
  cy.get('#mat-chip-list-0').click();
  cy.get(':nth-child(3) > .ng-invalid > .dropdown > .dropdown-trigger > .dropdown-label').click();
  cy.get(':nth-child(3) > .ng-invalid > .dropdown > .menu > .menu-content > .mat-selection-list > :nth-child(3) > .mat-list-item-content > .mat-pseudo-checkbox').click();
  cy.get('[aria-selected="true"] > .mat-list-item-content > .mat-pseudo-checkbox').click();
  cy.get(':nth-child(3) > .ng-invalid > .dropdown > .menu > .menu-actions > .btn').click();
  cy.get(':nth-child(3) > .ng-invalid > .dropdown > .dropdown-trigger > .dropdown-label').click();
  cy.get(':nth-child(3) > .ng-invalid > .dropdown > .menu > .menu-content > .mat-selection-list > :nth-child(3) > .mat-list-item-content > .mat-pseudo-checkbox').click();
  cy.get(':nth-child(3) > .ng-invalid > .dropdown > .menu > .menu-actions > .btn').click();
  cy.get(':nth-child(4) > .ng-invalid > .dropdown > .dropdown-trigger > .far').click();
  cy.get(':nth-child(16) > .mat-list-item-content > .mat-pseudo-checkbox').click();
  cy.get(':nth-child(4) > .ng-invalid > .dropdown > .menu > .menu-actions > .btn').click();
  cy.get('.ng-invalid > .dropdown > .dropdown-trigger > .far').click();
  cy.get('.ng-invalid > .dropdown > .menu > .menu-content > .mat-selection-list > .mat-list-item > .mat-list-item-content > .mat-pseudo-checkbox').click();
  cy.get('.ng-invalid > .dropdown > .menu > .menu-actions > .btn').click();
  cy.get('[type="submit"]').click();
  cy.get('.subnav--active > a > .nav-item__arrow > .far').click();
  /* ==== End Cypress Studio ==== */
});
