describe('Edit an exisiting note', function() {
    it('Makes an assertion', function() {
        cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
        cy.get('#mat-input-0').clear();
        cy.get('#mat-input-0').type('agentf');
        cy.get('#mat-input-1').clear();
        cy.get('#mat-input-1').type('Pa55word{enter}');
        cy.get('button').click();
        cy.get('.delay-0 > .conversation-list-item__wrapper > .conversation-list-item__container > .conversation-details > .details__conversation-subject').click();
        cy.get('.background--circle > .fas').click();
        cy.get('.ql-container > .ql-editor > p').clear().type('Edited Test Note 1'); 
        cy.get('.btn-send').click();
        cy.get('.presence__status--choice').click({force: true});
    })
})

// Line 11 - clears text already in text box then types the message