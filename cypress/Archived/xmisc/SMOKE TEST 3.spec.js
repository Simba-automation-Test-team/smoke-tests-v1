// SMOKE TEST 3.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
/* ==== Test Created with Cypress Studio ==== */
it('smoke test 3', function() {
  /* ==== Generated with Cypress Studio ==== */
  cy.visit('    https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('SUPERADMIN');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('superadmin');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click();
  cy.get('app-inner-nav > .nav__left > ol > :nth-child(1) > a').click();
  cy.get('[routerlink="people/users"]').click();
  cy.get('.logout').click({force: true});
  cy.request('localhost:8080');
  cy.get('#simba_internal-icon').click();
  cy.get('#simba_firstName').click().focus({force: true});
  cy.get('#simba_firstName').type('test');
  cy.get('#simba_lastName').clear();
  cy.get('#simba_lastName').type('testing');
  cy.get('#simba_phone').clear();
  cy.get('#simba_phone').type('07838682277');
  cy.get('#simba_prechatForm').click();
  cy.get('#simba_email').clear();
  cy.get('#simba_email').type('curtis@stellaruk.co.uk');
  cy.get('#simba_enquiry').click();
  cy.get('#simba_enquiry').type('This is a test');
  cy.get('#simba_submitBtn').click();
  /* ==== End Cypress Studio ==== */
});
