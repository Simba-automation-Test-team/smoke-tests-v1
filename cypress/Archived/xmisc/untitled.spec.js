describe('NAME OF TEST SUITE', () => {
    it('NAME OF TEST', () => {
        cy.request('localhost:8080');
        cy.get('#simba_internal-icon').click();
        cy.get('#simba_firstName').click().focus({force: true});
        cy.get('#simba_firstName').type('test');
        cy.get('#simba_lastName').clear();
        cy.get('#simba_lastName').type('testing');
        cy.get('#simba_phone').clear();
        cy.get('#simba_phone').type('07838682277');
        cy.get('#simba_prechatForm').click();
        cy.get('#simba_email').clear();
        cy.get('#simba_email').type('curtis@stellaruk.co.uk');
        cy.get('#simba_enquiry').click();
        cy.get('#simba_enquiry').type('This is a test');
        cy.get('#simba_submitBtn').click();
    });

    /* ==== Test Created with Cypress Studio ==== */
    it('Troubleshooting webchat', function() {
        /* ==== Generated with Cypress Studio ==== */
        cy.visit('localhost:8080');
        cy.wait(9000)
        cy.get('.display-name > a').click({force: true});
        cy.get('.top-row > .right > .close').click({force: true});
        /* ==== End Cypress Studio ==== */
    });
})
