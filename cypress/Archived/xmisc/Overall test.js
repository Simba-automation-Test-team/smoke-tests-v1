describe('My First Test', () => {
    it('Does not do much!', () => {
        cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
        cy.get('#mat-input-0').click();
        cy.get('#mat-input-0').type('superadmin');
        cy.get('#mat-input-1').click();
        cy.get('#mat-input-1').type('Pa55word');
        cy.get('button').click();
        cy.get('.form').submit();
        cy.get('.presence__status--choice').click({force: true});
        
    })

    /* ==== Test Created with Cypress Studio ==== */
    it('overall test ', function() {
        /* ==== Generated with Cypress Studio ==== */
        cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
        cy.get('#mat-input-0').clear();
        cy.get('#mat-input-0').type('superadmin');
        cy.get('#mat-input-1').clear();
        cy.get('#mat-input-1').type('Pa55word');
        cy.get('button').click();
        cy.get('app-inner-nav > .nav__left > ol > :nth-child(3) > a').click();
        cy.get('.nav-item--parent.subnav--visible > a').click();
        cy.get('app-inner-nav > .nav__left > ol > :nth-child(1) > a').click();
        cy.get('[routerlink="people/manage-skills"]').click();
        cy.get('.btn').click();
        cy.get(':nth-child(1) > .input--default').clear();
        cy.get(':nth-child(1) > .input--default').type('test');
        cy.get('.ng-invalid.ng-dirty > :nth-child(2) > .input--default').click();
        cy.get('.w-100 > .dropdown > .dropdown-trigger > .dropdown-label').click();
        cy.get('#mat-radio-3 > .mat-radio-label > .mat-radio-label-content').click();
        cy.get('#mat-radio-3-input').check();
        cy.get('.menu-actions > .btn').click();
        cy.get('.users > .dropdown > .dropdown-trigger > .dropdown-label > div.ng-star-inserted > .ng-star-inserted').click();
        cy.get('.users > .dropdown > .menu > .menu-content > .mat-selection-list > :nth-child(2) > .mat-list-item-content > .mat-pseudo-checkbox').click();
        cy.get('.users > .dropdown > .menu > .p-10 > .btn').click();
        cy.get('.queues > .dropdown > .dropdown-trigger > .dropdown-label > div.ng-star-inserted').click();
        cy.get('.queues > .dropdown > .menu > .menu-content > .mat-selection-list > :nth-child(2) > .mat-list-item-content > .mat-pseudo-checkbox').click();
        cy.get('.queues > .dropdown > .menu > .p-10 > .btn').click();
        cy.get('[type="submit"]').click();
        cy.visit('Webchat widget.html');
        cy.get('#simba_internal-icon').click();
        cy.get('#simba_firstName').click().focus({force: true});
        cy.get('#simba_firstName').type('test');
        cy.get('#simba_lastName').clear();
        cy.get('#simba_lastName').type('testing');
        cy.get('#simba_phone').clear();
        cy.get('#simba_phone').type('07838682277');
        cy.get('#simba_prechatForm').click();
        cy.get('#simba_email').clear();
        cy.get('#simba_email').type('curtis@stellaruk.co.uk');
        cy.get('#simba_enquiry').click();
        cy.get('#simba_enquiry').type('This is a test');
        cy.get('#simba_submitBtn').click();
        cy.get(':nth-child(1) > .input--default').clear();
        cy.get(':nth-child(1) > .input--default').type('testsmokeing');
        cy.get('[type="submit"]').click();
        cy.get('.logout').click({force: true});
        /* ==== End Cypress Studio ==== */
    });
})