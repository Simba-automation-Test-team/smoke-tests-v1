// setupTelephony.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
/* ==== Test Created with Cypress Studio ==== */
it.only('telephony setup', function() {
  /* ==== Generated with Cypress Studio ==== */
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear({force: true});
  cy.get('#mat-input-0').type('superadmin');
  cy.get('#mat-input-1').clear({force: true});
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click({force: true});
  cy.get('app-inner-nav > .nav__left > ol > :nth-child(3) > a').click({force: true});
  cy.get('[routerlink="platform/manage-channels"]').click({force: true});
  cy.get('app-telephony-channels.ng-star-inserted > .table-row__wrapper > .contains-list > :nth-child(3) > .quick-action > .quick-action__icon > .mat-tooltip-trigger').click({force: true});
  cy.get('#telepohnyChannelInstanceName').clear({force: true});
  cy.get('#telepohnyChannelInstanceName').type(userID_Alpha())
  function userID_Alpha() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  
      for (var i = 0; i < 10; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
  
      return text;
    } 
  cy.get('.form__wrapper > :nth-child(2) > .form__row > .row--stretch > .selection--custom').click({force: true});
  cy.get('#telephony-1808943 > .mat-radio-label').click({force: true});
  cy.get('#telephony-1808943-input').check({force: true});
  cy.get('.selection__component--footer > .btn').click({force: true});
  cy.get(':nth-child(1) > .form__row > .row--stretch > .selection--custom').click();
  cy.get('#simbaqueue-Fallback > .mat-radio-label > .mat-radio-container > .mat-radio-outer-circle').click({force: true});
  cy.get('#simbaqueue-Fallback-input').check({force: true});
  cy.get('.selection__component--footer > .btn').click({force: true});
  cy.get('section > :nth-child(2) > .form__row > .row--stretch > .selection--custom').click({force: true});
  cy.get('#cirrusQueue-185013 > .mat-radio-label > .mat-radio-container > .mat-radio-outer-circle').click({force: true});
  cy.get('.selection__component--footer > .btn').click({force: true});
  cy.get('.btn--complete').click({force: true});
  cy.get('.complete-screen__final-actions > .btn').click({force: true});
  cy.get('.logout').click({force: true});
  /* ==== End Cypress Studio ==== */
});
