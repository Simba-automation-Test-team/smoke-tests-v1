describe('Send a New Email to Consumer', function () {
    it('makes an assertion', function () {
        cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
        cy.get('#mat-input-0').clear();
        cy.get('#mat-input-0').type('agentf');
        cy.get('#mat-input-1').clear();
        cy.get('#mat-input-1').type('Pa55word');
        cy.get('button').click();
        cy.get('.delay-0 > .conversation-list-item__wrapper > .conversation-list-item__container > .conversation-details').click();
        cy.get('.consumer-profile-widget > .btn').click();
        cy.get('.customer-information > :nth-child(1) > label > .input--default').clear();
        cy.get('.customer-information > :nth-child(1) > label > .input--default').type('Mr');
        cy.get(':nth-child(2) > label > .input--default').clear();
        cy.get(':nth-child(2) > label > .input--default').type('FirTest');
        cy.get(':nth-child(3) > label > .input--default').clear();
        cy.get(':nth-child(3) > label > .input--default').type('SurTest');
        cy.get('.consumer-edit > :nth-child(4)').click();
        cy.get('.presence__status--choice').click({force: true});
    })
})
