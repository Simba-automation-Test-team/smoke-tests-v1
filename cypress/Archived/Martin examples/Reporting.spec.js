// logon.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
it('login', () => {
	cy.visit("https://test.simba.eks.omnichan.co.uk/auth/login")
	cy.get('input[placeholder="username"]').type('superadmin')
	cy.get('input[placeholder="password"]').type('Pa55word')
	cy.contains('L O G I N').click()
})

it('Reporting', () => {

		cy.contains("Reporting").click()
		cy.contains("Reports").click()
		cy.contains("User Presence").click()
		cy.contains("Reports").click()
		cy.contains("Contact Volume").click()
		cy.contains("Reports").click()
		cy.contains("SLA Adherence").click()
		cy.contains("Reports").click()
		cy.contains("Conversation Statuses").click()
//		cy.contains("Customer Satisfaction").click()
		cy.contains("Reports").click()
		cy.contains("Billing").click()
		cy.contains("Reports").click()
		// you can also use xpath full.
		cy.xpath("/html/body/app-root/div/app-admin/div/div/div/app-reporting/div/app-reports/div/div/app-reports-list/div/table/tbody/tr[7]/td/a").click() // tags
		//cy.contains("Tags").click()
})
