// sidemenu.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
it('login', () => {
	cy.visit("https://test.simba.eks.omnichan.co.uk/auth/login")
	cy.get('input[placeholder="username"]').type('superadmin')
	cy.get('input[placeholder="password"]').type('Pa55word')
	cy.contains('L O G I N').click()
})


it('sidemenu', () => {
		cy.get('[routerlink="/personal-queue"] > a > .fas').click()
		cy.get('[routerlink="/team-queue"] > a > .fas').click()
//		cy.get('[routerlink="pi-dashboard"]').click()
		cy.get('[routerlink="/admin"]').click()
})

