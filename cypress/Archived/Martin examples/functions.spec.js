// logon.spec.js created with Cypress
//
const StatusActive = 0
const StatusTest = 1
const StatusAway = 2
const StatusBusy = 3
const StatusLoggedOut = 4
const StatusUnknown = -1

const statusActiveField = "/html/body/app-root/app-header/header/div[1]/app-presence-statuses/div/div[2]/div[1]/span"
const statusTestField =   "/html/body/app-root/app-header/header/div[1]/app-presence-statuses/div/div[2]/div[2]/span"
const statusAwayField =   "/html/body/app-root/app-header/header/div[1]/app-presence-statuses/div/div[2]/div[3]/span"
const statusBusyField =   "/html/body/app-root/app-header/header/div[1]/app-presence-statuses/div/div[2]/div[4]/span"
const logoutField =       "/html/body/app-root/app-header/header/div[1]/app-presence-statuses/div/div[2]/div[5]/span"

const url = "https://test.simba.eks.omnichan.co.uk/auth/login"

export function logon(userID, password) {
	cy.log("Logging on as ", userID)
	cy.visit(url)
	cy.xpath("/html/body/app-root/div/app-login/div/div[1]/div[2]/form/div[1]/input").type(userID)
	cy.xpath("/html/body/app-root/div/app-login/div/div[1]/div[2]/form/div[2]/input").type(password)
	cy.xpath("/html/body/app-root/div/app-login/div/div[1]/div[2]/form/div[4]/button").click()
}

export function logoff () {
	cy.log("Logging off")
	setStatus(StatusLoggedOut)
}


export function getStatus() {
    cy.xpath("/html/body/app-root/app-header/header/div[1]/app-presence-statuses/div/div[1]/span").click()
	          
	if (cy.contains('Busy')) {
		return StatusBusy
	}
	if (cy.contains('Test')) {
		return StatusTest
	}
	if (cy.contains('Active')) {
		return StatusActive
	}
	if (cy.contains('Test')) {
		return StatusTest
	}
	return StatusUnknown
}

export function setStatus(newStatus) {
	cy.log("setting status to ", newStatus);
	cy.get('.presences').debug().get('.presence-item').should('have.length', 5)
	cy.get('.presences').debug().get('.presence-item').each(($el, index, $list) => {
		if (index == newStatus) {
			cy.wrap($el).click({ force: true })
			cy.log("Setting to ", index)
		}		
	})
	
}

