// logon.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
import {logon, logoff, setStatus, getStatus} from "./functions.spec.js";

const StatusActive = 0
const StatusTest = 1
const StatusAway = 2
const StatusBusy = 3
const StatusLoggedOut = 4
const StatusUnknown = -1


it('testlogin', () => {
	logon('superadmin','Pa55word');
})


it('status', () => {
	cy.log(getStatus())
	setStatus(StatusActive)
	cy.log(getStatus())
	setStatus(StatusBusy)
	cy.log(getStatus())
	setStatus(StatusAway)
	cy.log(getStatus())
	setStatus(StatusTest)
	cy.log(getStatus())
	logoff()
})

