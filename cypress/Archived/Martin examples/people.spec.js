// logon.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

import {logon, logoff, setStatus, getStatus} from "./functions.spec.js";
const PeopleField = "/html/body/app-root/div/app-admin/div/div/app-inner-nav/nav/ol/li[1]/a/div[1]/span"
const UsersField =  "/html/body/app-root/div/app-admin/div/div/app-inner-nav/nav/ol/li[2]/ul/li[1]/span"
const GroupsField = "/html/body/app-root/div/app-admin/div/div/app-inner-nav/nav/ol/li[2]/ul/li[2]/span"
const RolesField =  "/html/body/app-root/div/app-admin/div/div/app-inner-nav/nav/ol/li[2]/ul/li[3]/span"
const SkillsField = "/html/body/app-root/div/app-admin/div/div/app-inner-nav/nav/ol/li[2]/ul/li[4]/span"

const StatusActive = 0
const StatusTest = 1
const StatusAway = 2
const StatusBusy = 3
const StatusLoggedOut = 4
const StatusUnknown = -1

it('login', () => {
	logon('superadmin','Pa55word');
})

it('People', () => {
	cy.xpath(PeopleField).click()
	cy.xpath(UsersField).click()
	cy.xpath(GroupsField).click()
	cy.xpath(RolesField).click()
	cy.xpath(SkillsField).click()
	cy.xpath(PeopleField).click()
})

it ('Add User', () => {
	logon('superadmin', 'Pa55word')
	cy.xpath(PeopleField).click()
	cy.xpath(UsersField).click()
	cy.xpath("/html/body/app-root/div/app-admin/div/div/div/app-new-manage-users/section/app-new-admin-header/div/div[2]/div[3]/button").click()	//add new user
	cy.xpath("/html/body/app-root/div/app-admin/div/div/div/app-new-manage-users/section/div/div/div/mat-sidenav-container/mat-sidenav/div/app-edit-user/app-sidebar-form-editor/div/div[2]/mat-tab-group/div/mat-tab-body/div/form/div[1]/input").type('billy')
	cy.xpath("/html/body/app-root/div/app-admin/div/div/div/app-new-manage-users/section/div/div/div/mat-sidenav-container/mat-sidenav/div/app-edit-user/app-sidebar-form-editor/div/div[2]/mat-tab-group/div/mat-tab-body/div/form/div[2]/input").type('boy')
	cy.xpath("/html/body/app-root/div/app-admin/div/div/div/app-new-manage-users/section/div/div/div/mat-sidenav-container/mat-sidenav/div/app-edit-user/app-sidebar-form-editor/div/div[2]/mat-tab-group/div/mat-tab-body/div/form/div[3]/input").type('billyboy')
	cy.xpath("/html/body/app-root/div/app-admin/div/div/div/app-new-manage-users/section/div/div/div/mat-sidenav-container/mat-sidenav/div/app-edit-user/app-sidebar-form-editor/div/div[2]/mat-tab-group/div/mat-tab-body/div/form/div[4]/input").type('billy.boy@gmail.com')
	
//	cy.xpath("/html/body/app-root/div/app-admin/div/div/div/app-new-manage-users/section/div/div/div/mat-sidenav-container/mat-sidenav/div/app-edit-user/app-sidebar-form-editor/div/div[2]/mat-tab-group/div/mat-tab-body/div/form/div[5]/app-roles-selection-list/app-selection-list/div/div[1]/span").click() // user role
//	cy.xpath("/html/body/app-root/div/app-admin/div/div/div/app-new-manage-users/section/div/div/div/mat-sidenav-container/mat-sidenav/div/app-edit-user/app-sidebar-form-editor/div/div[2]/mat-tab-group/div/mat-tab-body/div/form/div[5]/app-roles-selection-list/app-selection-list/div/div[2]/div[2]/mat-selection-list/mat-list-option[4]/div/mat-pseudo-checkbox").click() // default agent
//	cy.xpath("/html/body/app-root/div/app-admin/div/div/div/app-new-manage-users/section/div/div/div/mat-sidenav-container/mat-sidenav/div/app-edit-user/app-sidebar-form-editor/div/div[2]/mat-tab-group/div/mat-tab-body/div/form/div[5]/app-roles-selection-list/app-selection-list/div/div[2]/div[3]/button").click() // done
	
//	cy.xpath("/html/body/app-root/div/app-admin/div/div/div/app-new-manage-users/section/div/div/div/mat-sidenav-container/mat-sidenav/div/app-edit-user/app-sidebar-form-editor/div/div[2]/mat-tab-group/div/mat-tab-body/div/form/div[6]/app-selectable-dropdown/div/div[1]/span").click() // add to group
//	cy.xpath("/html/body/app-root/div/app-admin/div/div/div/app-new-manage-users/section/div/div/div/mat-sidenav-container/mat-sidenav/div/app-edit-user/app-sidebar-form-editor/div/div[2]/mat-tab-group/div/mat-tab-body/div/form/div[6]/app-selectable-dropdown/div/div[2]/div[2]/mat-selection-list/mat-list-option[2]/div/mat-pseudo-checkbox").click() // first group
//	cy.xpath("//*[@id=\"mat-tab-content-0-0\"]/div/form/div[6]/app-selectable-dropdown/div/div[2]/div[3]/button").click() // done
	cy.contains("Create User").click({force: true})
	//cy.xpath("/html/body/app-root/div/app-admin/div/div/div/app-new-manage-users/section/div/div/div/mat-sidenav-container/mat-sidenav/div/app-edit-user/app-sidebar-form-editor/div/div[2]/mat-tab-group/div/mat-tab-body/div/form/div[10]/button[2]").click() // create user button
//	cy.xpath("//*[@id=\"mat-tab-content-0-0\"]/div/form/div[10]/button[2]")
})

