// logon.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
it('login', () => {
	cy.visit("https://test.simba.eks.omnichan.co.uk/auth/login")
	cy.get('input[placeholder="username"]').type('superadmin')
	cy.get('input[placeholder="password"]').type('Pa55word')
	cy.contains('L O G I N').click()
})
it('Platform', () => {
		cy.contains("Platform").click()
		cy.get('[routerlink="platform/marketplace"] > span').click()
		cy.get('[routerlink="platform/manage-channels"] > span').click()
		cy.get('[routerlink="platform/manage-hours"] > span').click()
		cy.get('[routerlink="platform/customer-satisfaction/surveys/active-surveys"] > span').click()
})

