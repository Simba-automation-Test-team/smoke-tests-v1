describe('Adding an address to Consumer profile', function() {
    it('Adds an address', function() {
        
        cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
        cy.get('#mat-input-0').clear().type('agentf');
        cy.get('#mat-input-1').clear().type('Pa55word{enter}');
        cy.get('button').click();
        //  Logging in
       
        cy.get('.delay-0 > .conversation-list-item__wrapper > .conversation-list-item__container > .conversation-details').click();
        cy.get('.consumer-profile-widget > .btn').click();
        cy.get('.customer-information > :nth-child(7)').click();
        //  Navigating to Conversations and Opening the Consumers Profile
        
        cy.get('[formarrayname="addresses"] > div.ng-untouched > :nth-child(3) > .ng-untouched').select('UK');
        cy.get('#addressBody').clear().type('3 The Roudal RoddingLaw Business Park');
        cy.get(':nth-child(4) > .input--default').clear().type('EH12 9DB');
        //  clear().type() clears text if there is any and then types the specified text
        
        cy.get('div.ng-valid > :nth-child(5) > .ng-valid').select('Office');
        cy.get('.consumer-edit > :nth-child(4)').click();
        //  Entering and saving the address
        
        cy.wait(1000)
        cy.get('.presence__status--choice').click({force: true});
        //  Logging out
    })
})
