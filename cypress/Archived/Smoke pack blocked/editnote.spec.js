describe('Edit an exisiting note', function() {
    it('edit existing note', function() {
        cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
        cy.get('#mat-input-0').clear();
        cy.get('#mat-input-0').type('agentf');
        cy.get('#mat-input-1').clear();
        cy.get('#mat-input-1').type('Pa55word{enter}');
        cy.get('button').click();
        
        //  Logging in
        cy.get('.delay-0 > .conversation-list-item__wrapper > .conversation-list-item__container > .conversation-details > .details__conversation-subject').click();
       
        //  Navigating and opening conversations , this line in particuar clears text already in text box then types the message
        
        cy.get('.background--circle').click({force: true});
        cy.get('[data-layer="Content"]').click({force: true}).type('Edited Test Note 1'); 
        cy.get('.btn-send').click({force: true});
       
        //  Editing and submiting Note changes
        
        cy.get('.presence__status--choice').click({force: true});
        // logging out
    })
})
