
/*This test will only function on the machine it was created- Curtis, Goldthorpe.
Additionally it will only function when the required HTML file(webchat widget) is being hosted on a local server*/


describe('Webchat replying ', function() {



it('Visitlocalhost', function() {
  cy.visit('http://192.168.0.10:8080');
 
  //*Below is the navigation for accessing the widget locally

  cy.get('.display-name > a').click();
  cy.get('#simba_internal-icon').click();
  
  //* below is the commands for typing in each field of the web-chat widget form

  cy.get('#simba_firstName').type('test');
  cy.get('#simba_lastName').type('this is a test');
  cy.get('#simba_email').type('curtis@stellaruk.co.uk');
  cy.get('#simba_enquiry').type('This is a test');
  
  //* below is the wait time for the message to land in simba

  cy.get('#simba_submitBtn').click().wait(1000);

});

it('back to simba', function(){
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login')
  
  //* Below is the Agent login information

  cy.get('#mat-input-0').type('agenta');
  cy.get('#mat-input-1').type('Pa55word');
  
  //*below is the login click
  
  cy.get('button').click();


  //* Below command is forcing the status to active
  
  cy.contains('Active').click({force: true});
  
 //* Below is a set of commands to navigate away from personal Queue and back to it

  cy.get('[routerlink="/team-queue"] > a > .fas').click().wait(1000);
  cy.get('[routerlink="/personal-queue"] > a > .fas').click();
  
  //* Below is the response to web-chat
  
  cy.get('.delay-0 > .conversation-list-item__wrapper > .conversation-list-item__container > .conversation-details').click();
  cy.get('.ql-container > .ql-editor').type('response');
  
  //*Below is the logout sequence
  
  cy.get('.btn-send').click();
  cy.get('.logout').click({force: true});
 
});

it('end webchat convo', function(){
  cy.visit('http://192.168.0.10:8080')
  
  //* Below is the commands for closing the webchat
  
  cy.get('.display-name > a').click().wait(2000);
  cy.get('.top-row > .right > .close').click();
  cy.get('.close-btn').click();
  
})

})
