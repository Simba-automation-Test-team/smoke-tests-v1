
/* ==== Test Created with Cypress Studio ==== */
it('search by FirstName', function() {
    /* ==== Generated with Cypress Studio ==== */
    cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
    cy.get('#mat-input-0').clear();
    cy.get('#mat-input-0').type('agentf');
    cy.get('#mat-input-1').clear();
    cy.get('#mat-input-1').type('Pa55word');
    cy.get('button').click();
    cy.get('.search-icon').click();
    cy.get('#mat-expansion-panel-header-2').click();
    cy.get('app-consumers-filter > .filter > :nth-child(1) > .input--default').clear();
    cy.get('app-consumers-filter > .filter > :nth-child(1) > .input--default').type('darren{enter}');
    cy.get('app-consumers-filter > .filter > .filter--buttons > .btn--save').click();
    cy.get('.results-item--column-70').click();
    cy.get('.presence__status--choice').click({force: true});
    /* ==== End Cypress Studio ==== */
  });