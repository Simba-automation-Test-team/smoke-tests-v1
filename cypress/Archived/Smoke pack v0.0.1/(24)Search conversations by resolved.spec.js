
it('Search conversations by Resolved', function() {
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('agentf');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word{enter}');
  cy.get('button').click();
  cy.get('.search-button').click();
  cy.get('#mat-expansion-panel-header-0 > .mat-content > .mat-expansion-panel-header-title').click();
  cy.get('[formarrayname="conversationStatuses"] > :nth-child(4)').click();
  cy.get(':nth-child(4) > .filter--checkbox').uncheck();
  cy.get('.filter--fieldset.ng-dirty > :nth-child(7)').click();
  cy.get(':nth-child(7) > .filter--checkbox').uncheck();
  cy.get('.filter--fieldset.ng-dirty > :nth-child(5)').click();
  cy.get(':nth-child(5) > .filter--checkbox').uncheck();
  cy.get('.filter--fieldset.ng-dirty > :nth-child(2)').click();
  cy.get(':nth-child(2) > .filter--checkbox').uncheck();
  cy.get('.filter--fieldset.ng-dirty').click();
  cy.get('.filter--fieldset.ng-dirty > :nth-child(3)').click();
  cy.get(':nth-child(3) > .filter--checkbox').uncheck();
  cy.get('app-conversations-filter > .filter > .filter--buttons > .btn--save').click();
  cy.get('.logout').click({force: true});
  
});
