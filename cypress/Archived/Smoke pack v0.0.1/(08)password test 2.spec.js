// password test 2.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
/* ==== Test Created with Cypress Studio ==== */
it('password set failed', function() {
  /* ==== Generated with Cypress Studio ==== */
  cy.visit('https://presales.simba.eks.omnichan.co.uk/reset-password?UzJo6EPwKSCDvQI2qaCt99uRUlnGZxwsPWg13153');
  cy.get(':nth-child(1) > .input--default').clear();
  cy.get(':nth-child(1) > .input--default').type('Pa55word');
  cy.get('.mt-10 > .input--default').clear();
  cy.get('.mt-10 > .input--default').type('Pa55word');
  cy.get('.login-page__btn').click();
  cy.get('.login-page__btn').click();
  cy.get('#mat-input-0').click();
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('WP');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click();
  /* ==== End Cypress Studio ==== */
});
