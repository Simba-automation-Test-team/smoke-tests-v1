// OpenCoversations_Agent.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
/* ==== Test Created with Cypress Studio ==== */
it('Coversation', function() {
  /* ==== Generated with Cypress Studio ==== */
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('agentf');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click();
  cy.get('.search-button').click();
  cy.get('#mat-expansion-panel-header-0').click();
  cy.get(':nth-child(2) > .filter--checkbox').type('29111999');
  cy.get(':nth-child(5) > .filter--checkbox').click();
  cy.get(':nth-child(6) > .filter--checkbox').click();
  cy.get(':nth-child(7) > .filter--checkbox').click();
  cy.get(':nth-child(4) > .filter--checkbox').click();
  cy.get('[formcontrolname="from"]').click();
  cy.get('app-conversations-filter > .filter > .filter--buttons > .btn--save').click();
 
 
  cy.get('.presence__status--choice').click({force: true});
  /* ==== End Cypress Studio ==== */
});
