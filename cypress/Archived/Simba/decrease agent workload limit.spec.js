
it('Decrease webchat workload limit', function() {
  
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login/');
  
  //*login 

  cy.get('#mat-input-0').type('superadmin');
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click();
  
  //*decreasing workload

  cy.get('app-inner-nav > .nav__left > ol > :nth-child(1) > a').click();
  cy.get('[routerlink="people/users"]').click();
  cy.get(':nth-child(23) > .table__item > :nth-child(4) > .cell__wrapper > :nth-child(1) > .mat-tooltip-trigger').click();
  cy.get('#\\37 c16fb4c-3a40-40f2-b606-b94be6c56c11').clear();
  cy.get('#\\37 c16fb4c-3a40-40f2-b606-b94be6c56c11').type('2');
  cy.get('.action-window-config__footer > .ng-star-inserted').click();
  cy.get('.complete-screen__final-actions > .btn').click();
  
  //*reseting test

  cy.get(':nth-child(23) > .table__item > :nth-child(4) > .cell__wrapper > :nth-child(1) > .mat-tooltip-trigger').click();
  cy.get('#\\37 c16fb4c-3a40-40f2-b606-b94be6c56c11').clear();
  cy.get('#\\37 c16fb4c-3a40-40f2-b606-b94be6c56c11').type('3');
  cy.get('.action-window-config__footer > .ng-star-inserted').click();
  cy.get('.complete-screen__final-actions > .btn').click();
  
  //*Logout
  
  cy.get('.logout').click({force: true});
  
});
