



describe('Smoke-test', function(){ 

    //#region Create_agent_user
    it('Create a agent user', function(){
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        let 
        website= 'https://presales.simba.eks.omnichan.co.uk/auth/login', 
        name='superadmin', 
        pass='Pa55word',
        fname='Curtis',
        lname='Goldthprpe',
        uname='agentsmoke02',
        
        email='agentsmoker02@placeholder.com'
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        cy.visit(website)
        cy.Login({name: name, pass: pass})



        
        cy.get('app-inner-nav > .nav__left > ol > :nth-child(1) > a').click();
        cy.get('[routerlink="people/users"]').click();
        cy.get('.btn').click();
        
        cy.get('#userName').type(uname);
       
        cy.get('#firstName').type(fname);
        
        cy.get('#lastName').type(lname);
        
        cy.get('#emailAddress').clear().type(email);
        
        
        cy.get('.roles-outer-container > .select-container > .closed-view > .placeholder-text').click();
        cy.get('#mat-checkbox-3 > .mat-checkbox-layout').click();
        cy.get('.button-wrapper > .btn').click();
        cy.get('.groups-outer-container > .select-container > .closed-view > .placeholder-text').click();
        cy.get('#mat-checkbox-15 > .mat-checkbox-layout > .mat-checkbox-inner-container').click();
        cy.get('#mat-checkbox-15-input').check();
        cy.get('.action-window').click();
        cy.get('.button-wrapper > .btn').click();
        cy.get('.placeholder-text').click();
        cy.get('#mat-checkbox-36 > .mat-checkbox-layout > .mat-checkbox-inner-container').click();
        cy.get('#mat-checkbox-36-input').check();
        cy.get('.button-wrapper > .btn').click();
        cy.get('.action-window-config__footer > .ng-star-inserted').click();
        cy.get('.complete-screen__final-actions > .btn').click();
        cy.get('.logout').click({force: true});
        
    })


})