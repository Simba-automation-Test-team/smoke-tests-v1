// Increase email workload.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
/* ==== Test Created with Cypress Studio ==== */
it('Increase agent Email workload', function() {
  /* ==== Generated with Cypress Studio ==== */
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login/');
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('superadmin');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word');
  cy.get('button').click();
  cy.get('app-inner-nav > .nav__left > ol > :nth-child(1) > a').click();
  cy.get('[routerlink="people/users"]').click();
  cy.get(':nth-child(23) > .table__item > :nth-child(4) > .cell__wrapper > :nth-child(1) > .mat-tooltip-trigger').click();
  cy.get('#\\32 2f07ca9-3140-4e94-92b9-3e92b0cdbeb5').clear();
  cy.get('#\\32 2f07ca9-3140-4e94-92b9-3e92b0cdbeb5').type('550');
  cy.get('.action-window-config__footer > .ng-star-inserted').click();
  cy.get('.complete-screen__final-actions > .btn').click();
  cy.get(':nth-child(23) > .table__item > :nth-child(4) > .cell__wrapper > :nth-child(1) > .mat-tooltip-trigger').click();
  cy.get('#\\32 2f07ca9-3140-4e94-92b9-3e92b0cdbeb5').clear();
  cy.get('#\\32 2f07ca9-3140-4e94-92b9-3e92b0cdbeb5').type('500');
  cy.get('.action-window-config__footer > .ng-star-inserted').click();
  cy.get('.complete-screen__final-actions > .btn').click();
  cy.get('.logout').click({force: true});
  /* ==== End Cypress Studio ==== */
});
