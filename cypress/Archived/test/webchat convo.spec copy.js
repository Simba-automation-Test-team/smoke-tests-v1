it('Visitlocalhost', function() {
  cy.visit('http://192.168.0.10:8080');
  /* ==== Generated with Cypress Studio ==== */
  cy.get('.display-name > a').click();
  cy.get('#simba_internal-icon').click();
  cy.get('#simba_firstName').clear();
  cy.get('#simba_firstName').type('test');
  cy.get('#simba_lastName').clear();
  cy.get('#simba_lastName').type('this is a test');
  cy.get('#simba_email').clear();
  cy.get('#simba_email').type('curtis@stellaruk.co.uk');
  cy.get('#simba_enquiry').type('This is a test');
  cy.get('#simba_submitBtn').click().wait(10000);
  /* ==== End Cypress Studio ==== */
});

it('back to simba', function(){
  cy.visit('https://presales.simba.eks.omnichan.co.uk/auth/login')
  /* ==== Generated with Cypress Studio ==== */
  cy.get('#mat-input-0').clear();
  cy.get('#mat-input-0').type('agenta');
  cy.get('#mat-input-1').clear();
  cy.get('#mat-input-1').type('Pa55word');

  
  cy.get('button').click();


  /* ==== Generated with Cypress Studio ==== */
  cy.get(':nth-child(1) > .presence__name').click({force: true});
  /* ==== End Cypress Studio ==== */
  /* ==== Generated with Cypress Studio ==== */
  cy.get('[routerlink="/team-queue"] > a > .fas').click().wait(1000);
  cy.get('[routerlink="/personal-queue"] > a > .fas').click();
  cy.get('.delay-0 > .conversation-list-item__wrapper > .conversation-list-item__container > .conversation-details').click();
  cy.get('.ql-container > .ql-editor').click();
  cy.get('.btn-send__content').click();
  /* ==== End Cypress Studio ==== */
  /* ==== Generated with Cypress Studio ==== */
  cy.get('.ql-container > .ql-editor').click().type('response');
  cy.get('.btn-send').click();
  cy.get('.logout').click({force: true});
 
});

it('end webchat convo', function(){
  cy.visit('http://192.168.0.10:8080')
  /* ==== Generated with Cypress Studio ==== */
  cy.get('.display-name > a').click();
  cy.get('.top-row > .right > .close').click();
  cy.get('.close-btn').click();
  /* ==== End Cypress Studio ==== */
})
